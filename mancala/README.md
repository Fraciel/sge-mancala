# Mancala Board

![Notation](libs/images/mancalaBoard.png)

### For more information about Mancala rules:

https://www.ymimports.com/pages/how-to-play-mancala

# Please also refer to the "SGE-Manual"

## Running a game of mancala

```
java -jar sge-1.0.5.jar match --file=mancala-1.0.jar --directory=agents
```

If you want specific agents to play against each other add them via --file
instead of --directory or use their Agent-Name like so:

```
java -jar sge-1.0.5.jar match --file=mancala-1.0.jar --directory=agents -a MctsAgent AlphaBetaAgent
```

Note that the agents play in that exact order. To limit the calculation time to n
seconds use --computation-time n or the equivalent short option -c n.

To disable the game ui use --disable-ui (ui only works with a match).
You can customize the board size, and the initial seed count using --board i,k

In order to use a play the game against an Agent, one should

It is highly recommended to use -v -v (twice) flag to make sge show the error text and stacktrace.

## Running a tournament

```
java -jar sge-1.0.5.jar tournament --file=mancala-1.0.jar --directory=agents --mode="Double_Round_Robin"
```

## Implementing an agent

The agent has to implement the Java-Interface at.ac.tuwien.ifs.sge.agent.GameAgent<?
extends Game<A, ?>, A> where A is the type of the action, provided by sge.

Before each turn the game is stripped from player-specific information and then
submitted to the agent via the computeNextAction() method, alongside the
allowed computational time as well as its unit.

Whatever this action returns is then applied to the canonical game via
doAction(). Note however that doAction() is side-effect free, as it returns a
new instance of the game, with that move applied. Mancala-Agents ought to return
objects of the class MancalaAction.

To access the board getBoard() can be used. This class has the whole Board to access (Which is a custom iterator),
it also has useful methods such as getBoardPointedTowardsPocket, getPocketsByPlayerId, getPocketByNotation.

Every pocket of the game is defined in the Pocket class, which holds the seed count, notation, ownerId, and a boolean
that suggests if the pocket is a House or not.

To get the possible actions of that turn, one can use the getPossibleActions method in the Game class.
A Mancala Action holds the starting Pocket to gather the seeds from in order to distribute to the following ones.

# Default Agents

SGE provides three default agents:
• RandomAgent
– An agent which chooses from all possible actions a random one.
• AlphaBetaAgent
– An agent which uses (aggressive) alpha beta pruning to determine
the next best action.
• MCTSAgent
– An agent which uses Monte-Carlo-Tree-Search to find the most likely
winning action.
Be wary however these agents are not implemented for Mancala directly but rather
use an abstract notion of games, so the heuristics are rather bad, i.e. they do
not model the game particularly well. Hence beating them might
not be too difficult.