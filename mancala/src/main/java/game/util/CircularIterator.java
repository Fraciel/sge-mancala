package game.util;

import game.board.Pocket;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CircularIterator implements Iterator<Pocket> {
    private final List<Pocket> pockets;
    private int currentIndex;

    public CircularIterator(List<Pocket> pockets) {
        this.pockets = pockets;
        this.currentIndex = 0;
    }

    public CircularIterator(CircularIterator iterator) {
        this.pockets = new ArrayList<>(iterator.pockets.size());
        for (Pocket pocket : iterator.pockets) {
            this.pockets.add(new Pocket(pocket));
        }
        this.currentIndex = iterator.currentIndex;
    }

    @Override
    public boolean hasNext() {
        return !pockets.isEmpty();
    }

    public Pocket getPocket() {
        return pockets.get(currentIndex);
    }

    public List<Pocket> getPockets() {
        return this.pockets;
    }

    public

    @Override
    Pocket next() {
        if (!hasNext()) {
            throw new IllegalStateException("No elements in the iterator.");
        }
        this.currentIndex = (this.currentIndex + 1) % pockets.size();
        return pockets.get(this.currentIndex);
    }

    public int size() {
        return pockets.size();
    }

    public Pocket getOpposite() {
        return pockets.get((pockets.size()) - currentIndex - 2);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int houseAIndex = (pockets.size() - 2) / 2;
        int houseBIndex = pockets.size() - 1;

        sb.append("BOARD: \n");
        sb.append("              ");
        for (int i = houseBIndex - 1; i > houseAIndex; i--) {
            sb.append(pockets.get(i).toString()).append("  ");
        }
        sb.append("\n");

        sb.append(pockets.get(houseBIndex).toString());

        sb.append("        ".repeat(pockets.size() / 2));

        sb.append(pockets.get(houseAIndex).toString())
                .append("\n");

        sb.append("              ");
        for (int i = 0; i < houseAIndex; i++) {
            sb.append(pockets.get(i).toString()).append("  ");
        }
        return sb.toString();
    }
}
