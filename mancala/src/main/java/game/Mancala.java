package game;

import at.ac.tuwien.ifs.sge.game.ActionRecord;
import at.ac.tuwien.ifs.sge.game.Game;
import game.UI.MancalaUI;
import game.board.MancalaAction;
import game.board.MancalaBoard;
import game.board.Pocket;
import game.util.CircularIterator;

import java.util.*;

public class Mancala implements Game<MancalaAction, MancalaBoard> {

    private final MancalaBoard mancalaBoard;

    private int currentPlayerId;

    private final boolean canonical;

    private final List<ActionRecord<MancalaAction>> actionRecords;

    private MancalaUI mancalaUI;


    public Mancala() {
        this.mancalaBoard = new MancalaBoard();
        this.canonical = true;
        this.actionRecords = new ArrayList<>();
        this.currentPlayerId = 0;
    }

    public Mancala(int currentPlayerId, boolean canonical, List<ActionRecord<MancalaAction>> actionRecords, MancalaBoard mancalaBoard) {
        this.currentPlayerId = currentPlayerId;
        this.canonical = canonical;
        this.actionRecords = actionRecords;
        this.mancalaBoard = mancalaBoard;
    }

    public Mancala(String board, int numberOfPlayers) {
        this(0, true, new ArrayList<>(), (board == null ? new MancalaBoard() : new MancalaBoard(getInitialPocketCountFromInputString(board), getInitialSeedCountFromInputString(board))));


        if (numberOfPlayers != 2) {
            throw new IllegalArgumentException("2 player game!");
        }
    }

    public Mancala(Mancala original) {
        this.currentPlayerId = original.currentPlayerId;
        this.canonical = original.canonical;
        this.actionRecords = new ArrayList<>(original.actionRecords);
        this.mancalaBoard = new MancalaBoard(original.mancalaBoard);
        this.mancalaUI = original.mancalaUI;
    }


    @Override
    public String toTextRepresentation() {
        return mancalaBoard.getBoard().toString();
    }

    @Override
    public boolean isGameOver() {
        return getBoard().isGameOver();
    }

    @Override
    public int getMinimumNumberOfPlayers() {
        return 2;
    }

    @Override
    public int getMaximumNumberOfPlayers() {
        return 2;
    }

    @Override
    public int getNumberOfPlayers() {
        return 2;
    }

    @Override
    public int getCurrentPlayer() {
        return currentPlayerId;
    }

    @Override
    public double getUtilityValue(int playerId) {
        return getBoard().getHouseByPlayerId(playerId).getSeeds();
    }

    @Override
    public Set<MancalaAction> getPossibleActions() {

        Set<MancalaAction> possibleActions = new TreeSet<>();
        CircularIterator board = getBoard().getBoard();
        for (int i = 0; i < board.size(); i++) {
            Pocket pocket = board.next();
            if (!pocket.isHouse() && pocket.belongsTo(this.currentPlayerId) && !pocket.isEmpty()) {
                possibleActions.add(new MancalaAction(pocket));
            }
        }

        return possibleActions;
    }

    @Override
    public MancalaBoard getBoard() {
        return mancalaBoard;
    }


    @Override
    public Game<MancalaAction, MancalaBoard> doAction(MancalaAction mancalaAction) {
        if (mancalaAction == null) {
            throw new IllegalArgumentException("Found null");
        }
        if (isGameOver()) {
            throw new IllegalArgumentException("Game is over");
        }

        Mancala next = new Mancala(this);

        CircularIterator board = next.getBoard().getBoardPointedTowardsPocket(mancalaAction.pocket());

        Pocket startingPocket = board.getPocket();
        int seedsToDistribute = startingPocket.getSeeds();
        startingPocket.removeSeeds();

        while (seedsToDistribute > 0) {
            Pocket currentPocket = board.next();
            if (!currentPocket.isHouse() || currentPocket.belongsTo(next.currentPlayerId)) {
                currentPocket.addSeed();
                seedsToDistribute--;
            }

            // If there are no remaining seeds and the last pocket is a house,
            // the player turn does not change.
            if (seedsToDistribute == 0 && !currentPocket.isHouse()) {

                // If the last pocket was empty before putting in the last seed,
                // special rule invokes and the opposite pocket gets raided.
                if (currentPocket.belongsTo(next.currentPlayerId) && currentPocket.getSeeds() == 1) {

                    int seedsToSteal = board.getOpposite().getSeeds();
                    board.getOpposite().removeSeeds();
                    Pocket house = next.getBoard().getHouseByPlayerId(next.currentPlayerId);
                    house.addSeeds(seedsToSteal);
                }
                next.currentPlayerId = 1 - next.currentPlayerId;
            }
        }


        if (next.isGameOver()) {
            next.getBoard().transferRemainingSeedsToHouses();
        }
        next.actionRecords.add(new ActionRecord<>(currentPlayerId, mancalaAction));

        return next;
    }


    @Override
    public MancalaAction determineNextAction() {
        return null;
    }

    @Override
    public List<ActionRecord<MancalaAction>> getActionRecords() {
        return Collections.unmodifiableList(actionRecords);
    }

    @Override
    public boolean isCanonical() {
        return canonical;
    }

    @Override
    public Game<MancalaAction, MancalaBoard> getGame(int i) {
        return this;
    }

    public static int getInitialPocketCountFromInputString(String board) {
        String[] parts = board.split(",");
        int pocketCount;

        try {
            pocketCount = Integer.parseInt(parts[0]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid pocket count: must be an integer");
        }

        if (pocketCount > 10) {
            throw new IllegalArgumentException("Cannot have more than 10 pockets!");
        }
        if (pocketCount < 4) {
            throw new IllegalArgumentException("Cannot have less than 4 pockets!");
        }
        return pocketCount;
    }

    public static int getInitialSeedCountFromInputString(String board) {
        String[] parts = board.split(",");
        int seedCount;

        try {
            seedCount = Integer.parseInt(parts[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid pocket count: must be an integer");
        }

        if (seedCount > 15) {
            throw new IllegalArgumentException("Cannot have more than 15 seeds!");
        }
        if (seedCount < 3) {
            throw new IllegalArgumentException("Cannot have less than 3 seeds!");
        }
        return seedCount;
    }

}
