package game.board;

public record MancalaAction(Pocket pocket) implements Comparable<MancalaAction> {

    @Override
    public int compareTo(MancalaAction mancalaAction) {
        return this.pocket.getNotation().compareTo(mancalaAction.pocket().getNotation());
    }

    @Override
    public String toString() {
        return pocket.getNotation();
    }
}
