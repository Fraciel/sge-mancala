package game.board;

import game.util.CircularIterator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

public class MancalaBoard {

    private final CircularIterator board;

    public MancalaBoard() {
        this(6, 4);
    }

    public MancalaBoard(final int numPockets, final int seedCount) {
        List<Pocket> pockets = new ArrayList<>();
        for (int i = 0; i < numPockets; i++) {
            pockets.add(new Pocket(seedCount, "A" + (i + 1), 0, false));
        }
        pockets.add(new Pocket(0, "House A", 0, true));

        for (int i = 0; i < numPockets; i++) {
            pockets.add(new Pocket(seedCount, "B" + (i + 1), 1, false));
        }
        pockets.add(new Pocket(0, "House B", 1, true));

        board = new CircularIterator(pockets);
    }

    public MancalaBoard(MancalaBoard original) {
        this.board = new CircularIterator(original.board);
    }

    public CircularIterator getBoard() {
        return board;
    }

    public boolean isGameOver() {
        return checkIfPocketsByPlayerIdAreEmpty(0) || checkIfPocketsByPlayerIdAreEmpty(1);
    }

    public void transferRemainingSeedsToHouses() {
        Pocket currentPocket = this.board.getPocket();
        int remainingSeeds = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < this.board.size(); j++) {
                if (currentPocket.belongsTo(i) && !currentPocket.isHouse()) {
                    remainingSeeds += currentPocket.getSeeds();
                    currentPocket.removeSeeds();
                }
                currentPocket = this.board.next();
            }
            Pocket playerHouse = getHouseByPlayerId(i);
            playerHouse.addSeeds(remainingSeeds);
            remainingSeeds = 0;
        }
    }

    public boolean checkIfPocketsByPlayerIdAreEmpty(int playerId) {
        for (int i = 0; i < board.size(); i++) {
            Pocket pocket = board.next();
            if (!pocket.isHouse() && pocket.belongsTo(playerId) && !pocket.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public CircularIterator getBoardPointedTowardsPocket(Pocket pocketToPointAt) {
        for (int i = 0; i < board.size(); i++) {
            Pocket pocket = board.next();
            if (Objects.equals(pocketToPointAt.toString(), pocket.toString())) {
                return board;
            }
        }
        throw new IllegalStateException("Cannot locate the pocket with the notation: " + pocketToPointAt.toString());
    }

    public List<Pocket> getPocketsByPlayerId(int playerId) {
        if (playerId == 0) {
            return board.getPockets().subList(0, ((board.size() / 2) - 1));
        } else {
            return board.getPockets().subList((board.size() / 2), (board.size() - 1));
        }
    }

    public Pocket getHouseByPlayerId(int playerId) {
        if (playerId == 0) {
            return getPocketByNotation("House A");
        } else {
            return getPocketByNotation("House B");
        }
    }

    public Pocket getPocketByNotation(String notation) {
        for (int i = 0; i < board.size(); i++) {
            if (notation.equals(board.getPockets().get(i).getNotation())) {
                return board.getPockets().get(i);
            }
        }
        throw new NoSuchElementException("No pocket found with the given notation:" + notation + ".");
    }

}
