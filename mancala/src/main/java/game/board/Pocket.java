package game.board;

public class Pocket {

    private int seeds;
    private final String notation;
    private final int ownerId;
    private final boolean isHouse;

    public Pocket(int seeds, String notation, int ownerId, boolean isHouse) {
        this.seeds = seeds;
        this.notation = notation;
        this.ownerId = ownerId;
        this.isHouse = isHouse;
    }

    public Pocket(Pocket pocket){
        this.seeds = pocket.seeds;
        this.notation = pocket.notation;
        this.ownerId = pocket.ownerId;
        this.isHouse = pocket.isHouse;
    }

    public void addSeed(){
        this.seeds += 1;
    }
    public void addSeeds(int seeds){
        this.seeds += seeds;
    }

    public void removeSeeds(){
        this.seeds = 0;
    }

    public boolean belongsTo(int playerId){
        return this.ownerId == playerId;
    }

    public boolean isHouse(){
        return isHouse;
    }

    public int getSeeds() {
        return seeds;
    }

    public boolean isEmpty(){
        return this.seeds == 0;
    }

    public String getNotation(){
        return this.notation;
    }

    @Override
    public String toString(){
        return getNotation() + ": " + getSeeds();
    }
}
