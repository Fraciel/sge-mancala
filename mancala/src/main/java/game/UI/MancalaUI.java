package game.UI;

import at.ac.tuwien.ifs.sge.engine.Logger;
import at.ac.tuwien.ifs.sge.engine.game.Match;
import at.ac.tuwien.ifs.sge.game.Game;
import at.ac.tuwien.ifs.sge.ui.GameUI;
import game.UI.panels.CenterPanel;
import game.UI.panels.FooterPanel;
import game.UI.panels.NotationPanel;
import game.UI.panels.TitlePanel;
import game.board.MancalaAction;
import game.board.MancalaBoard;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class MancalaUI extends JFrame implements GameUI<MancalaAction> {

    private Game<MancalaAction, MancalaBoard> game;
    private List<?> agents;

    private CenterPanel centerPanel;
    private FooterPanel footerPanel;
    private NotationPanel notationPanel;
    private BlockingQueue<MancalaAction> actionQueue;

    @Override
    @SuppressWarnings("unchecked")
    public void startUI(Match<?, ?, ?> match) {
        this.game = (Game<MancalaAction, MancalaBoard>) match.getGame();
        this.actionQueue = (BlockingQueue<MancalaAction>) match.getActionQueue();
        this.agents = match.getGameAgents();

        this.setLayout(new BorderLayout());
        this.setTitle("Mancala");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setSize(((this.game.getBoard().getBoard().size() / 2) * 100) + 400, 650);

        this.add(new TitlePanel(), BorderLayout.NORTH);

        this.footerPanel = new FooterPanel(match.getComputationTime());
        this.footerPanel.startTimer();
        this.footerPanel.resetTimer(getCurrentAgentName());
        this.add(footerPanel, BorderLayout.SOUTH);

        this.centerPanel = new CenterPanel(this, agents, game);
        this.add(this.centerPanel, BorderLayout.CENTER);

        this.notationPanel = new NotationPanel();
        this.add(this.notationPanel, BorderLayout.EAST);

        this.setVisible(true);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void updateUI(Game<?, ?> game) {
        this.game = (Game<MancalaAction, MancalaBoard>) game;
        this.centerPanel.updateButtons(this.game);
        this.footerPanel.resetTimer(getCurrentAgentName());
        this.notationPanel.updateNotations(this.game.getActionRecords());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void endGame(Game<?, ?> game) {
        this.game = (Game<MancalaAction, MancalaBoard>) game;
        this.centerPanel.getHouseAPanel().restoreButtonColor();
        this.centerPanel.getHouseBPanel().restoreButtonColor();
        this.footerPanel.endGame(this.game);
    }

    @Override
    public MancalaAction getNextAction() throws InterruptedException {
        return actionQueue.take();
    }

    public void submitAction(MancalaAction mancalaAction) {
        if (actionQueue.size() <= 1) {
            actionQueue.offer(mancalaAction);
        }
    }

    public MancalaUI(Logger log) {
    }

    public String getCurrentAgentName() {
        return agents.get(game.getCurrentPlayer()).getClass().getSimpleName();
    }

}
