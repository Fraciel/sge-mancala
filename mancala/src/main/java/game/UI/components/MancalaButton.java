package game.UI.components;

import at.ac.tuwien.ifs.sge.game.Game;
import game.UI.MancalaUI;
import game.board.MancalaAction;
import game.board.MancalaBoard;
import game.board.Pocket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MancalaButton extends JButton {

    private Pocket pocket;
    private final JLabel notationLabel;
    private final JLabel seedsLabel;
    private Color borderColor;
    private final MancalaUI ui;


    public MancalaButton(MancalaUI ui, Pocket pocket, boolean enabled) {
        this.pocket = pocket;
        this.ui = ui;
        this.setEnabled(enabled);

        this.setBackground(Colors.buttonBackground);
        if (isEnabled()) {
            this.borderColor = Colors.buttonCurrentPlayerColor;
        } else {
            this.borderColor = Colors.buttonForeground;
        }

        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
        this.setFocusable(false);
        this.setContentAreaFilled(false);

        this.setLayout(new BorderLayout());
        this.addActionListener(e -> {
            if (ui != null) {
                ui.submitAction(new MancalaAction(pocket));
            }
        });

        this.notationLabel = new JLabel(pocket.getNotation() + ":", SwingConstants.CENTER);
        this.notationLabel.setForeground(isEnabled() ? Colors.buttonCurrentPlayerColor : Colors.textColor);
        this.notationLabel.setFont(new Font("Arial", Font.PLAIN, 14));

        this.seedsLabel = new JLabel(String.valueOf(pocket.getSeeds()), SwingConstants.CENTER);
        this.seedsLabel.setForeground(isEnabled() ? Colors.buttonCurrentPlayerColor : Colors.textColor);
        this.seedsLabel.setFont(new Font("Arial", Font.BOLD, 30));

        if (pocket.belongsTo(0) && pocket.isHouse()) {
            changeButtonColor(Colors.buttonCurrentPlayerColor);
        }

        this.add(notationLabel, BorderLayout.NORTH);
        this.add(seedsLabel, BorderLayout.CENTER);
    }

    public void updateButton(Game<MancalaAction, MancalaBoard> game, int belongsTo) {
        this.pocket = game.getBoard().getPocketByNotation(this.pocket.getNotation());

        notationLabel.setText(this.pocket.getNotation() + ":");
        seedsLabel.setText(String.valueOf(this.pocket.getSeeds()));
        this.setEnabled(this.pocket.getSeeds() != 0 && game.getCurrentPlayer() == belongsTo && !pocket.isHouse());
        this.notationLabel.setForeground(isEnabled() ? Colors.buttonCurrentPlayerColor : Colors.textColor);
        this.seedsLabel.setForeground(isEnabled() ? Colors.buttonCurrentPlayerColor : Colors.textColor);
        if (isEnabled()) {
            this.borderColor = Colors.buttonCurrentPlayerColor;
        } else {
            this.borderColor = Colors.buttonForeground;
        }
        for (ActionListener al : this.getActionListeners()) {
            this.removeActionListener(al);
        }
        this.addActionListener(e -> {
            if (ui != null) {
                ui.submitAction(new MancalaAction(game.getBoard().getPocketByNotation(pocket.getNotation())));
            }
        });

        if (pocket.isHouse() && game.getCurrentPlayer() == belongsTo) {
            changeButtonColor(Colors.buttonCurrentPlayerColor);
        }

        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (getModel().isPressed()) {
            g2.setColor(getBackground().darker());
        } else if (getModel().isRollover()) {
            g2.setColor(getBackground().brighter());
        } else {
            g2.setColor(getBackground());
        }

        g2.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
        super.paintComponent(g2);

        g2.dispose();
    }

    @Override
    protected void paintBorder(Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(this.borderColor);
        g2.setStroke(new BasicStroke(3));
        g2.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 10, 10);
        g2.dispose();
    }

    public void changeButtonBorderColor(Color color) {
        this.borderColor = color;
        repaint();
    }

    public void changeButtonTextColor(Color color) {
        this.notationLabel.setForeground(color);
        this.seedsLabel.setForeground(color);
        repaint();
    }

    public void changeButtonColor(Color color) {
        this.borderColor = color;
        this.notationLabel.setForeground(color);
        this.seedsLabel.setForeground(color);
    }

}
