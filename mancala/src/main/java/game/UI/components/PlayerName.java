package game.UI.components;

import javax.swing.*;
import java.awt.*;

public class PlayerName extends JPanel {

    public PlayerName(String agentName, String borderLayout, int horizontalLayout) {
        this.setLayout(new BorderLayout());
        this.setBackground(Colors.background);

        JPanel playerNamePanel = new JPanel(new BorderLayout());
        playerNamePanel.setBackground(Colors.background);

        JPanel containerPanel = new JPanel(new GridBagLayout());
        containerPanel.setBackground(Colors.background);
        containerPanel.setPreferredSize(new Dimension(1000, 100));

        JLabel nameLabel = new JLabel(agentName);
        nameLabel.setForeground(Colors.textColor);
        nameLabel.setHorizontalAlignment(horizontalLayout);
        playerNamePanel.add(nameLabel, borderLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;

        containerPanel.add(playerNamePanel, gbc);

        this.add(containerPanel, borderLayout);

    }
}
