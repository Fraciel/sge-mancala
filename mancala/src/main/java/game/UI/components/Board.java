package game.UI.components;

import at.ac.tuwien.ifs.sge.game.Game;
import game.UI.MancalaUI;
import game.board.MancalaAction;
import game.board.MancalaBoard;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Board extends JPanel {

    private final PlayerBoard player1Board;
    private final PlayerBoard player2Board;

    public Board(MancalaUI ui, Game<MancalaAction, MancalaBoard> game, List<?> agents) {
        this.setLayout(new BorderLayout());
        this.setBackground(Colors.background);

        PlayerName player2Name = new PlayerName("Player 2: " + agents.get(1).getClass().getSimpleName(), BorderLayout.SOUTH, SwingConstants.LEFT);
        PlayerName player1Name = new PlayerName("Player 1: " + agents.get(0).getClass().getSimpleName(), BorderLayout.NORTH, SwingConstants.RIGHT);

        this.add(player2Name, BorderLayout.NORTH);
        this.add(player1Name, BorderLayout.SOUTH);

        JPanel board = new JPanel();
        board.setBackground(Colors.background);

        board.setLayout(new BoxLayout(board, BoxLayout.Y_AXIS));


        player2Board = new PlayerBoard(ui, game, 1, BorderLayout.SOUTH);
        player1Board = new PlayerBoard(ui, game, 0, BorderLayout.NORTH);

        board.add(Box.createVerticalGlue());
        board.add(player2Board, BorderLayout.NORTH);
        board.add(player1Board, BorderLayout.SOUTH);
        board.add(Box.createVerticalGlue());

        this.add(board, BorderLayout.CENTER);
    }

    public void updateButtons(Game<MancalaAction, MancalaBoard> game) {
        player1Board.updateButtons(game, 0);
        player2Board.updateButtons(game, 1);
    }
}
