package game.UI.components;

import at.ac.tuwien.ifs.sge.game.Game;
import game.UI.MancalaUI;
import game.board.MancalaAction;
import game.board.MancalaBoard;
import game.board.Pocket;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerBoard extends JPanel {

    List<MancalaButton> playerButtons = new ArrayList<>();


    public PlayerBoard(MancalaUI ui, Game<MancalaAction, MancalaBoard> game, int playerId, String buttonPanelPosition) {
        this.setLayout(new BorderLayout());
        this.setBackground(Colors.background);
        this.setPreferredSize(new Dimension(1000, 150));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setBackground(Colors.background);
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));

        List<Pocket> playerPockets = game.getBoard().getPocketsByPlayerId(playerId);
        if (playerId == 0) {
            for (Pocket playerPocket : playerPockets) {
                MancalaButton mancalaButton = new MancalaButton(ui, playerPocket, (game.getCurrentPlayer() == 0) && (playerPocket.getSeeds() != 0));
                mancalaButton.setPreferredSize(new Dimension(75, 75));
                playerButtons.add(mancalaButton);
                buttonPanel.add(mancalaButton, new GridBagConstraints());
            }
        } else {
            for (int i = playerPockets.size() - 1; i >= 0; i--) {
                MancalaButton mancalaButton = new MancalaButton(ui, playerPockets.get(i), (game.getCurrentPlayer() == 1) && (playerPockets.get(i).getSeeds() != 0));
                mancalaButton.setPreferredSize(new Dimension(75, 75));
                playerButtons.add(mancalaButton);
                buttonPanel.add(mancalaButton, new GridBagConstraints());
            }
        }

        this.add(buttonPanel, buttonPanelPosition);

    }

    public void updateButtons(Game<MancalaAction, MancalaBoard> game, int belongsTo) {
        for (MancalaButton playerButton : playerButtons) {
            playerButton.updateButton(game, belongsTo);
        }
    }
}
