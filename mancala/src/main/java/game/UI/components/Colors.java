package game.UI.components;

import java.awt.*;

public class Colors {
    public static Color background = new Color(250, 249, 240);
    public static Color textColor = new Color(76, 35, 10);

    public static Color buttonBackground = new Color(245, 243, 224);
    public static Color buttonForeground = new Color(43, 65, 98);
    public static Color buttonCurrentPlayerColor = new Color(58, 125, 68);

}
