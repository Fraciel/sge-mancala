package game.UI.panels;

import at.ac.tuwien.ifs.sge.game.ActionRecord;
import game.UI.components.Colors;
import game.board.MancalaAction;

import javax.swing.*;
import java.awt.*;

public class NotationPanel extends JPanel {

    private final JTextArea notationArea;

    public NotationPanel() {
        this.setBackground(Colors.background);
        this.setPreferredSize(new Dimension(150, 650));
        this.setLayout(new BorderLayout());

        JTextArea titleArea = new JTextArea();
        titleArea.setText("Action Records:\n\n");
        titleArea.setFont(new Font("Arial", Font.BOLD, 16));
        titleArea.setBackground(Colors.background);
        titleArea.setForeground(Colors.textColor);
        titleArea.setEditable(false);

        this.notationArea = new JTextArea();
        this.notationArea.setBackground(Colors.background);
        this.notationArea.setForeground(Colors.textColor);
        this.notationArea.setFont(new Font("Arial", Font.BOLD, 14));
        this.notationArea.setEditable(false);
        this.notationArea.setLineWrap(true);
        this.notationArea.setWrapStyleWord(true);
        this.notationArea.setBorder(BorderFactory.createEmptyBorder());
        this.notationArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(this.notationArea);
        scrollPane.setBackground(Colors.background);
        scrollPane.setPreferredSize(new Dimension(150, 300));
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 15));

        this.add(titleArea, BorderLayout.NORTH);
        this.add(scrollPane, BorderLayout.CENTER);


    }

    public void updateNotations(java.util.List<ActionRecord<MancalaAction>> actionRecords) {
        StringBuilder notations = new StringBuilder();
        for (ActionRecord<MancalaAction> actionRecord : actionRecords) {
            String action = actionRecord.getAction().pocket().getNotation();
            int playerId = actionRecord.getPlayer() + 1;

            notations.append("Player ").append(playerId).append(":  ").append(action).append("\n");

        }
        notationArea.setText(notations.toString());
    }
}
