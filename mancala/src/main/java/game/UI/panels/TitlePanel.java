package game.UI.panels;

import game.UI.components.Colors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class TitlePanel extends JPanel {

    public TitlePanel() {
        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(200, 100));
        this.setBackground(Colors.background);

        JLabel gameTitle = new JLabel();
        gameTitle.setText("Mancala");
        gameTitle.setFont(new Font("Montserrat", Font.BOLD, 32));
        gameTitle.setForeground(Colors.textColor);
        gameTitle.setHorizontalAlignment(JLabel.CENTER);
        gameTitle.setBorder(new EmptyBorder(15, 20, 20, 20));
        JPanel titlePanel = new JPanel();
        titlePanel.setBackground(Colors.background);
        titlePanel.add(gameTitle);

        this.add(titlePanel, BorderLayout.CENTER);
    }

}
