package game.UI.panels;

import at.ac.tuwien.ifs.sge.game.Game;
import game.UI.MancalaUI;
import game.UI.components.Board;
import game.UI.components.Colors;
import game.board.MancalaAction;
import game.board.MancalaBoard;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class CenterPanel extends JPanel {

    private final HousePanel houseBPanel;
    private final HousePanel houseAPanel;
    private final Board boardPanel;

    public CenterPanel(MancalaUI ui, List<?> agents, Game<MancalaAction, MancalaBoard> game) {
        this.setLayout(new BorderLayout());
        this.setBackground(Colors.background);

        houseAPanel = new HousePanel(game.getBoard().getPocketByNotation("House A"), GridBagConstraints.WEST);
        this.add(houseAPanel, BorderLayout.EAST);

        houseBPanel = new HousePanel(game.getBoard().getPocketByNotation("House B"), GridBagConstraints.EAST);
        this.add(houseBPanel, BorderLayout.WEST);

        boardPanel = new Board(ui, game, agents);
        this.add(boardPanel, BorderLayout.CENTER);
    }

    public void updateButtons(Game<MancalaAction, MancalaBoard> game) {
        houseAPanel.updateButton(game, 0);
        houseBPanel.updateButton(game, 1);
        boardPanel.updateButtons(game);
    }

    public HousePanel getHouseBPanel() {
        return houseBPanel;
    }

    public HousePanel getHouseAPanel() {
        return houseAPanel;
    }
}
