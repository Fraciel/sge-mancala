package game.UI.panels;

import at.ac.tuwien.ifs.sge.game.Game;
import game.UI.components.Colors;
import game.board.MancalaAction;
import game.board.MancalaBoard;

import javax.swing.*;
import java.awt.*;

public class FooterPanel extends JPanel {

    private long counter;
    private final long computationTime;
    private boolean isHuman;
    private boolean gameEnded = false;

    public FooterPanel(long computationTime) {
        this.setPreferredSize(new Dimension(200, 100));
        this.setBackground(Colors.background);
        this.computationTime = computationTime;

        this.counter = this.computationTime;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.setColor(Colors.textColor);

        if (!isHuman && !gameEnded) {
            String timeString = "Time Left: " + counter + " s";
            g.drawString(timeString, getWidth() / 2 - g.getFontMetrics().stringWidth(timeString) / 2, getHeight() / 2);
        }
    }

    public void startTimer() {
        Timer timer = new Timer(1000, e -> {
            if (this.counter > 0) {
                this.counter--;
                repaint();
            }
        });
        timer.start();
    }

    public void resetTimer(String currentAgent) {
        isHuman = currentAgent.equals("HumanAgent");
        counter = computationTime;
    }

    public void endGame(Game<MancalaAction, MancalaBoard> game) {
        gameEnded = true;
        JTextArea resultText = new JTextArea();
        resultText.setFont(new Font("Arial", Font.BOLD, 16));
        resultText.setBackground(Colors.background);
        resultText.setForeground(Colors.textColor);
        resultText.setEditable(false);
        StringBuilder gameResult = new StringBuilder();
        int player1Score = game.getBoard().getHouseByPlayerId(0).getSeeds();
        int player2Score = game.getBoard().getHouseByPlayerId(1).getSeeds();

        if (player1Score < player2Score) {
            gameResult.append("Player 2 Wins!!!");
        } else {
            gameResult.append("Player 1 Wins!!!");
        }

        resultText.setText(gameResult.toString());
        this.removeAll();
        this.add(resultText, BorderLayout.CENTER);
        this.revalidate();
        this.repaint();
    }
}
