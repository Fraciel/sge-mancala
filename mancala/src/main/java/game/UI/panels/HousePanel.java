package game.UI.panels;

import at.ac.tuwien.ifs.sge.game.Game;
import game.UI.components.Colors;
import game.UI.components.MancalaButton;
import game.board.MancalaAction;
import game.board.MancalaBoard;
import game.board.Pocket;

import javax.swing.*;
import java.awt.*;

public class HousePanel extends JPanel {

    MancalaButton houseButton;

    public HousePanel(Pocket house, int side) {
        this.setLayout(new GridBagLayout());
        this.setBackground(Colors.background);
        this.setPreferredSize(new Dimension(150, 600));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = side;
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(10, 10, 10, 10);

        this.houseButton = new MancalaButton(null, house, false);
        this.add(houseButton, gbc);
    }

    public void updateButton(Game<MancalaAction, MancalaBoard> game, int belongsTo) {
        this.houseButton.updateButton(game, belongsTo);
    }

    public void restoreButtonColor() {
        this.houseButton.changeButtonBorderColor(Colors.buttonForeground);
        this.houseButton.changeButtonTextColor(Colors.textColor);
    }
}
